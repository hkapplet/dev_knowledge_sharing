# Development knowledge sharing

**Development knowledge sharing**

1. **Springboot development** 
*  Use Auto-configuration
*  Use Spring Initializr for starting new Spring Boot projects.
*  Controllers should be stateless! Controllers are by default singletons and giving them any state can cause massive issues.
*  Controllers should not execute business logic but rely on delegation.
*  Controllers should deal with the HTTP layer of the application. This should not be passed down to Services.
*  Controllers should be oriented around a use-case / business-capability.
*  Build your @Service's around business capabilities.
*  @Autowired annotation optional on constructors, you also get the benefit of being able to easily instantiate your bean without Spring.
*  Provide global exception handling (Ref: [link](https://www.baeldung.com/exception-handling-for-rest-with-spring))
*   1.  You should use HandlerExceptionResolver for defining your global exception handling strategy.
    2.  You can annotate your Controllers with @ExceptionHandler. This can come useful if you want to be specific in certain cases.
*  Use a logging framework
*   e.g Logger logger = LoggerFactory.getLogger(MyClass.class); 
*   



Reference: 
1. Spring Boot – Best Practices (E4JDeveloper)[https://www.e4developer.com/2018/08/06/spring-boot-best-practices/](https://www.e4developer.com/2018/08/06/spring-boot-best-practices/)
2. Spring Boot 2.0 — Project Structure and Best Practices (Part 2) [https://medium.com/the-resonant-web/spring-boot-2-0-project-structure-and-best-practices-part-2-7137bdcba7d3](https://medium.com/the-resonant-web/spring-boot-2-0-project-structure-and-best-practices-part-2-7137bdcba7d3)
3. Spring Boot 2.0 — Project Structure and Best Practices (Part 1) (https://medium.com/the-resonant-web/spring-boot-2-0-starter-kit-part-1-23ddff0c7da2)
4. springboot-starterkit.git [https://github.com/khandelwal-arpit/springboot-starterkit.git](https://github.com/khandelwal-arpit/springboot-starterkit.git)
